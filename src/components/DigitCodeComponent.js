import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input'

const DigitCodeComponent = ({ setValue }) => {
  return (
    <SafeAreaView>
      <OTPInputView
        style={{ width: '85%', height: 200, alignSelf: 'center' }}
        pinCount={4}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={(code => setValue(code))}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  underlineStyleBase: {
    width: 40,
    height: 45,
    borderWidth: 0,
    borderWidth: 1,
    borderColor: 'black',
    fontSize: 20,
    color: 'black'
  },
  underlineStyleHighLighted: {
    borderColor: "#17C4EA",
  },
});

export default DigitCodeComponent;
