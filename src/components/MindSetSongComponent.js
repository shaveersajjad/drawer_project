import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScaledSheet } from 'react-native-size-matters';

const MindSetSongComponent = ({ title, trackTime, trackDescription, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.MindSetSongContainer}>
      <View style={styles.contentContainer}>
        <View style={styles.timeBtnContainer}>
          <Text style={styles.trackTime}>{trackTime}</Text>
        </View>
        <Text style={styles.trackName}>{title}</Text>
        <Text style={styles.trackContent}>{trackDescription}</Text>
      </View>
      <View style={styles.playIconContainer}>
        <Icon name="play" size={20} color="white" />
      </View>
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  MindSetSongContainer: {
    backgroundColor: 'white',
    borderRadius: '4@s',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: '6@s',
    padding: '10@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 3,
  },
  contentContainer: {
    width: '77%',
  },
  timeBtnContainer: {
    backgroundColor: '#BDFFEF',
    borderRadius: '4@s',
    paddingVertical: 6,
    width: '60@s',
    alignItems: 'center',
    marginBottom: '6@s',
    justifyContent: 'center',
  },
  trackTime: {
    fontSize: '13@s',
    color: '#000',
  },
  playIconContainer: {
    height: '40@s',
    width: '40@s',
    borderRadius: '100@s',
    backgroundColor: '#17C4EA',
    alignItems: 'center',
    justifyContent: 'center',
  },
  trackName: {
    fontSize: '14@s',
    fontWeight: '500',
  },
  trackContent: {
    fontSize: '11@s',
    fontWeight: '400',
    color: '#575757',
  },
});

export default MindSetSongComponent;
