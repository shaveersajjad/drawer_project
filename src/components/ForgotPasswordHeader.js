import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {ScaledSheet} from 'react-native-size-matters';

const ForgotPasswordHeader = ({headerTitle, goBack}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => goBack()}>
        <Icon name="chevron-left" color="#00404E" size={30} />
      </TouchableOpacity>
      <Text style={styles.headerTitle}>{headerTitle}</Text>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '10@s',
  },
  headerTitle: {
    fontSize: '14@s',
    fontWeight: '400',
    color: 'white',
  },
});

export default ForgotPasswordHeader;
