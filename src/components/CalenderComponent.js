import React from 'react';
import { Text, View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

const CalenderComponent = ({ headerTitle }) => {
  const vacation = { key: 'vacation', color: 'red', selectedDotColor: 'blue' };
  const massage = { key: 'massage', color: 'blue', selectedDotColor: 'blue' };
  const workout = { key: 'workout', color: 'green' };
  return (
    <View style={styles.container}>
      <Calendar
        style={{
          backgroundColor: '#D2F7FF',
          // borderRadius: 20,
          paddingBottom: 20,
          // marginTop: 10,
        }}
        markingType={'multi-dot'}
        markedDates={{
          '2017-10-25': {
            dots: [vacation, massage, workout],
            selected: true,
            selectedColor: 'red',
          },
          '2017-10-26': { dots: [massage, workout], disabled: true },
        }}
        theme={{
          calendarBackground: '#D2F7FF',
          monthTextColor: 'black',
          dayTextColor: 'black',
          textDisabledColor: 'grey',
          textDayFontWeight: '700',
          arrowColor: '#17C4EA',
          'stylesheet.calendar.header': {
            week: {
              marginTop: 5,
              flexDirection: 'row',
              justifyContent: 'space-between',
            },
          },
        }}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: '#fff',

    // flexDirection: 'row',
    // alignItems: 'center',
    // justifyContent: 'space-between',
    // padding: '15@s',
  },
  headerTitle: {
    fontSize: '14@s',
    fontWeight: '400',
    color: 'white',
  },
});

export default CalenderComponent;
