import React, { useState, useEffect } from 'react';
import { Text, TouchableOpacity, View, Platform, TextInput } from 'react-native';
import { useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScaledSheet } from 'react-native-size-matters';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

const MealTimeComponent = ({ title, children }) => {
  const storeData = useSelector(({ meals }) => Object.entries(meals.meals).length === 0 ? {} : meals?.meals?.[0]);

  useEffect(() => {
  }, [added]);

  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const [added, setAdded] = useState();
  const [itemValue, setItemValue] = useState("");

  let mappingData;
  if (Object.entries(storeData).length === 0)
    mappingData = [];
  else {
    const dataIndex = Object.keys(storeData).indexOf(title.toLowerCase());
    const values = Object.values(storeData);
    mappingData = values?.[dataIndex]?.items;
  }

  const onChange = (event, selectedDate) => {
    setDate(selectedDate);
  };

  const showMode = () => {
    setShow(!show);
  };

  const handleAddItem = () => {
    mappingData.push("");
    console.log("mapp", mappingData)
    setAdded(!added);
  };

  const handleChange = value => {
    setItemValue(value);
  };

  const handleSubmit = () => {
    console.log("value", itemValue);
  }

  return (
    <View style={styles.planMealContainer}>
      <View style={styles.mealTimeContentBar} >
        {children}
        <Text style={styles.breakfastText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={showMode} style={styles.timePickerMainContainer}>
        <View style={styles.timePickerContainer}>
          <View style={styles.timeHoursBorder}>
            <Text style={styles.timeText}>{moment(date).format('hh')}</Text>
          </View>
          <Text style={styles.dotText}>:</Text>
          <View style={styles.timeHoursBorder}>
            <Text style={styles.timeText}>{moment(date).format('m')}</Text>
          </View>
          <View style={styles.timeAmPmBorder}>
            <Text style={styles.timeText}>{moment(date).format('A')}</Text>
          </View>
        </View>
        <Icon style={styles.dropdownIcon} name="caret-down" size={28} color="#17C4EA" />
      </TouchableOpacity>
      <View>
        {show && (
          <DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode="time"
            is24Hour={true}
            display={"spinner"}
            is24Hour={false}
            onChange={onChange}
            style={{ backgroundColor: "#DCF6FC", borderRadius: 40 }}
          />
        )}
      </View>
      {mappingData.length !== 0 ?
        mappingData.map(data => (
          <TextInput
            style={styles.textInputStyle}
            placeholder="Add Item"
            value={data == "" ? itemValue : data}
            editable={data == "" ? true : false}
            onChangeText={value => handleChange(value)}
            onEndEditing={() => handleSubmit()}
          />
        )) :
        <TextInput style={styles.textInputStyle} placeholder="Add Item" />
      }
      <TouchableOpacity style={styles.addMoreItemsContainer} onPress={() => handleAddItem()} >
        <Text style={styles.addIcon}>+</Text>
        <Text style={styles.addMoreText}>Add More Items</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  planMealContainer: {
    paddingHorizontal: '15@s',
    marginTop: '20@s',
  },
  mealTimeContentBar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  breakfastText: {
    fontSize: '14@s',
    fontWeight: '600',
    color: 'black',
    marginLeft: '6@s',
  },
  timePickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '12@s',
  },
  timeHoursBorder: {
    borderWidth: 1,
    borderColor: '#17C4EA',
    width: '30%',
    padding: '6@s',
    borderRadius: '4@s',
    alignItems: 'center'
  },
  timeAmPmBorder: {
    borderWidth: 1,
    borderColor: '#17C4EA',
    width: '16%',
    padding: '6@s',
    borderRadius: '4@s',
    alignItems: 'center',
    marginLeft: '7@s',
  },
  dotText: {
    fontSize: '16@s',
    paddingHorizontal: '2@s',
    fontWeight: '600',
    color: 'black',
  },
  timeText: {
    fontSize: '14@s',
    fontWeight: '500',
    color: 'black',
  },
  timePickerMainContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '7@s',
  },
  dropdownIcon: {
    marginTop: '11@s',
    marginRight: '13@s'
  },
  textInputStyle: {
    backgroundColor: 'rgba(23, 196, 234, 0.15);',
    width: '100%',
    marginTop: '12@s',
    paddingLeft: '15@s',
    color: 'black',
    fontWeight: '500',
    fontSize: '14@s',
    padding: '6@s',
    borderRadius: '4@s'
  },
  addMoreItemsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '7@s'
  },
  addIcon: {
    fontSize: '15@s',
    marginRight: '3@s',
    fontWeight: '400'
  },
  addMoreText: {
    fontSize: '9@s',
    color: 'black',
    fontStyle: 'normal',
    fontWeight: '500'
  }
});

export default MealTimeComponent;