import React from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

const WeekDaysComponent = ({title}) => {
  return (
    <TouchableOpacity style={styles.weekDaybtnContainer}>
      <Text style={styles.weekDayNames}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  weekDaybtnContainer: {
    borderWidth: 1,
    borderColor: '#17C4EA',
    marginRight: '5@s',
    marginTop: '7@s',
    borderRadius: '4@s',
    paddingVertical: 8,
    paddingHorizontal: 18,
  },
  weekDayNames: {
    fontSize: '14@s',
    fontWeight: '400',
    fontStyle: 'normal',
    color: 'black',
  },
});

export default WeekDaysComponent;
