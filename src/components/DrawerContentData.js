import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';

const DrawercontentData = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.drawermainContainer}>
        <View style={styles.drawerHeader}>
          <Icon name="chevron-left" size={20} />
          <Text style={styles.drawerLogo}>MWB</Text>
          <View></View>
        </View>
        <View style={{ marginTop: 30 }}>
          <View style={styles.contentFlex}>
            <Text style={styles.contentText}>Planned Meals</Text>
            <Icon name="arrow-right" size={20} />
          </View>
          <View style={styles.contentFlex}>
            <Text style={styles.contentText}>Meditation</Text>
            <Icon name="arrow-right" size={20} />
          </View>
          <View style={styles.contentFlex}>
            <Text style={styles.contentText}>About Us</Text>
            <Icon name="arrow-right" size={20} />
          </View>
        </View>
      </View>
      <View style={styles.btnContainer}>
        <TouchableOpacity style={styles.logoutbtContainer}>
          <Text style={styles.logoutText}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  drawerHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '25@s',
  },
  contentFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 30,
  },
  drawermainContainer: {
    flex: 1,
  },
  drawerLogo: {
    fontSize: '16@s',
    fontWeight: '700',
  },
  contentText: {
    fontSize: '16@s',
    fontWeight: '600',
  },
  logoutbtContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: '12@s',
    height: '80@s',
  },
  logoutText: {
    fontSize: '16@s',
    fontStyle: 'normal',
    fontWeight: '600',
    lineHeight: '19@s',
  },
});

export default DrawercontentData;
