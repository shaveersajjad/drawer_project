import React from 'react';
import { View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Timeline from 'react-native-timeline-flatlist';
import CollapseComponent from './CollapseComponent';

const TimeLineComponent = ({ meals }) => {
  const concatTime = (time) => {
    const hours = time?.hour > 9 ? (time?.hour).toString() : `0${time?.hour}`;
    const minutes = time?.minutes > 9 ? (time?.minutes).toString() : `0${time?.minutes}`;
    return hours + ' ' + minutes;
  };

  const data = [
    {
      time: concatTime(meals && meals?.breakfast?.time),
      title: 'Breakfast',
      items: meals?.breakfast?.items
    },
    {
      time: concatTime(meals && meals?.lunch?.time),
      title: 'Lunch',
      items: meals?.lunch?.items
    },
    {
      time: concatTime(meals && meals?.dinner?.time),
      title: 'Dinner',
      items: meals?.dinner?.items
    },
  ];

  const renderDetail = (rowData) => {
    return <CollapseComponent meals={rowData && rowData} />
  };

  return (
    <View style={styles.container}>
      <Timeline
        style={styles.list}
        data={data}
        timeStyle={{
          textAlign: 'center',
          backgroundColor: '#17C4EA',
          color: 'white',
          padding: 2,
          borderRadius: 10,
        }}
        options={{
          style: { paddingTop: 5 },
        }}
        innerCircle={'icon'}
        renderFullLine={true}
        circleSize={16}
        lineWidth={8}
        lineColor="rgb(45,156,219)"
        detailContainerStyle={{ marginTop: -15 }}
        renderDetail={renderDetail}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: 'white'
  },
});

export default TimeLineComponent;