import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { ScaledSheet } from 'react-native-size-matters';

const MenuHeaderComponent = ({ headerTitle, navigate, openDrawer }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={openDrawer}>
        <Icon name="bars" color="#fff" size={25} />
      </TouchableOpacity>
      <Text style={styles.headerTitle}>{headerTitle}</Text>
      <TouchableOpacity onPress={() => navigate('Notification')}>
        <Icon name="bell" color="#fff" size={25} />
      </TouchableOpacity>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: '#17C4EA',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '15@s',
  },
  headerTitle: {
    fontSize: '14@s',
    fontWeight: '400',
    color: 'white',
  },
});

export default MenuHeaderComponent;
