import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {
  Collapse,
  CollapseHeader,
  CollapseBody,
  AccordionList,
} from 'accordion-collapse-react-native';

const CollapseComponent = ({ meals }) => {
  console.log("meals", meals)
  return (
    <View>
      <Collapse style={styles.collapseContainer}>
        <CollapseHeader>
          <View style={styles.contentFlex}>
            <Icon name="coffee" size={15} />
            <Text style={styles.mealDayTime}>{meals?.title}</Text>
          </View>
          <View style={styles.iconBar}>
            <Text style={styles.textInputStyle} >{meals?.items?.[0]}</Text>
            <Icon name="caret-down" color="#17C4EA" size={30} />
          </View>
        </CollapseHeader>
        <CollapseBody>
          {
            meals?.items?.length > 1 && meals?.items.map(item, index => <Text style={styles.textInputStyle} >{item}</Text>)
          }
          <View style={styles.editBtnBar}>
            <View></View>
            <TouchableOpacity style={styles.editBtnContainer}>
              <Text style={styles.editBtnText}>Edit</Text>
            </TouchableOpacity>
          </View>
        </CollapseBody>
      </Collapse>
    </View>
  );
};

const styles = ScaledSheet.create({
  collapseContainer: {
    backgroundColor: 'white',
    borderRadius: '4@s',
    padding: '10@s',
    margin: '20@s',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,

    elevation: 16,
  },
  contentFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: '6@s',
  },
  iconBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mealDayTime: {
    fontSize: '14@s',
    fontWeight: '500',
    marginLeft: '5@s',
  },
  textInputStyle: {
    backgroundColor: 'rgba(23, 196, 234, 0.15);',
    width: '87%',
    marginTop: '10@s',
    paddingLeft: '10@s',
    color: '#00404E',
    fontWeight: '500',
    fontSize: '14@s',
    padding: '4@s',
    borderRadius: '4@s',
    marginLeft: '6@s',
  },
  editBtnBar: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '88%',
    justifyContent: 'space-between',
  },
  editBtnContainer: {
    backgroundColor: '#17C4EA',
    paddingHorizontal: 28,
    paddingVertical: 8,
    borderRadius: '4@s',
    marginTop: '10@s',
  },
  editBtnText: {
    fontSize: '11@s',
    fontWeight: '500',
    color: 'white',
  },
});

export default CollapseComponent;