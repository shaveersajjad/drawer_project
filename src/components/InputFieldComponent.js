import React from 'react';
import { View, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { ScaledSheet } from 'react-native-size-matters';

const InputFieldComponent = ({ placeholder, iconName, name, value, handleChange }) => {
  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        placeholderTextColor="black"
        value={value}
        onChangeText={value => handleChange(name, value)}
        autoCapitalize='none'
      />
      <Icon name={iconName} color="#0A84FF" size={35} />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    backgroundColor: '#fff',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: '10@s',
    borderRadius: '4@s',
  },
  input: {
    padding: '10@s',
    color: '#8E8E93',
    width: '100%',
    backgroundColor: 'white',
    marginLeft: '2@s',
    fontSize: '14@s',
    color: "black",
  },
});

export default InputFieldComponent;
