import React from 'react';
import {Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

const NotificationComponent = ({title}) => {
  return (
    <View style={styles.notificationcontainer}>
      <Text style={styles.btnText}>{title}</Text>
    </View>
  );
};

const styles = ScaledSheet.create({
  notificationcontainer: {
    backgroundColor: '#D2F7FF',
    borderRadius: '4@s',
    marginTop: '8@s',
    padding: '16@s',
  },
  btnText: {
    fontSize: '12@s',
    fontWeight: '400',
    color: '#000',
  },
});

export default NotificationComponent;
