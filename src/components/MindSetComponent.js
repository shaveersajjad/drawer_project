import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

const MindSetComponent = ({title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.Btncontainer}>
      <Text style={styles.btnText}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  Btncontainer: {
    backgroundColor: '#D2F7FF',
    borderRadius: '4@s',
    height: '40@s',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '6@s',
    paddingHorizontal: 25,
  },
  btnText: {
    fontSize: '14@s',
    fontWeight: '400',
    color: '#575757',
  },
});

export default MindSetComponent;
