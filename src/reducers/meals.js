import createReducer from '../store/createReducer';
import * as types from '../actions/types';

export const meals = createReducer({
  meals: {},
},
  {
    [types.GET_ALL_MEALS](state, action) {
      return {
        ...state,
        meals: action?.meals
      };
    },
  },
);