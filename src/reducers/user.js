import createReducer from '../store/createReducer';
import * as types from '../actions/types';

export const user = createReducer({
  user: {},
},
  {
    [types.USER](state, action) {
      return {
        ...state,
        user: action?.user
      };
    },
    [types.PAYMENTS](state, action) {
      return {
        ...state,
        payments: action?.payments
      };
    },
  },
);