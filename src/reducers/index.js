import { combineReducers } from 'redux';

import * as userReducer from './user';
import * as mealsReducer from './meals';

const appReducer = combineReducers(Object.assign(userReducer, mealsReducer));

const rootReducer = (state, action) => {
  // if (action.type === 'LOGOUT') {
  //   return appReducer({}, action)
  // }
  return appReducer(state, action)
};

export default rootReducer;