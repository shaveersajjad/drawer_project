import React, {useState} from 'react';
import {View} from 'react-native';
import {useDispatch} from 'react-redux';
import {ScaledSheet} from 'react-native-size-matters';

import {SignUpScreen} from '../screens';
import {signUp} from '../actions/auth';

const SignUpContainer = props => {
  const goBack = () => {
    const {navigation} = props;
    navigation.goBack();
  };
  const dispatch = useDispatch();

  const [signUpData, setSignUpData] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
  });
  const [animation, setAnimation] = useState(false);

  const navigate = routeName => {
    const {navigation} = props;
    navigation.navigate(routeName);
  };

  const handleSignUpData = (name, value) => {
    setSignUpData({
      ...signUpData,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    dispatch(signUp(signUpData, navigate, setAnimation));
  };

  return (
    <View style={styles.container}>
      <SignUpScreen
        signUpData={signUpData}
        handleSignUpData={handleSignUpData}
        handleSubmit={handleSubmit}
        navigate={navigate}
        animation={animation}
        goBack={goBack}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default SignUpContainer;
