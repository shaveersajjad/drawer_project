import React, { useEffect } from 'react';
import { View } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

import Storage from '../Utils/Storage';
import { RouteCheckingScreen } from '../screens';

const RouteCheckingContainer = props => {
  useEffect(() => {
    token();
  }, [props.navigation]);

  const token = async () => {
    const userToken = await Storage.retrieveData('token')
      .then(token => {
        return token
      });
    props.navigation.reset({
      index: 0,
      routes: [{
        name: userToken == null ? 'Login' : 'Home',
      }],
    });
  };

  return (
    <View style={styles.container}>
      <RouteCheckingScreen />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
});

export default RouteCheckingContainer;