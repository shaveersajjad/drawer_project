import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ScaledSheet } from 'react-native-size-matters';

import Storage from '../Utils/Storage';
import { HomeScreen } from '../screens';
import { getUserData } from '../actions/auth';
import { getAllMeals } from '../actions/meals';

const HomeContainer = (props) => {
  const dispatch = useDispatch();

  const meals = useSelector(({ meals }) =>
    Object.entries(meals.meals).length === 0 ? {} : meals?.meals?.[0]
  );

  useEffect(() => {
    userData();
    dispatch(getAllMeals());
  }, []);

  const userData = async () => {
    await Storage.retrieveData('currentUser').then((userData) =>
      dispatch(getUserData(userData?._id))
    );
  };

  const navigate = (routeName) => {
    const { navigation } = props;
    navigation.navigate(routeName);
  };

  const goBack = () => {
    const { navigation } = props;
    navigation.goBack();
  };

  const openDrawer = () => {
    props.navigation.openDrawer();
  };

  return (
    <View style={styles.container}>
      <HomeScreen openDrawer={openDrawer} navigate={navigate} goBack={goBack} meals={meals} />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default HomeContainer;
