import React, { useState } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { ScaledSheet } from 'react-native-size-matters';
import Toast from 'react-native-toast-message';

import NewPasswordScreen from '../screens/NewPasswordScreen';
import { resetPassword } from '../actions/auth';

const NewPasswordContainer = props => {
  const dispatch = useDispatch();

  const [resetPasswordData, setResetPasswordData] = useState({
    password: '',
    confirmPassword: '',
  });
  const [animation, setAnimation] = useState(false);

  const handlePassword = (name, value) => {
    setResetPasswordData({
      ...resetPasswordData,
      [name]: value,
    });
  };

  const navigate = routeName => {
    const { navigation } = props;
    navigation.navigate(routeName);
  };

  const goBack = () => {
    const { navigation } = props;
    navigation.goBack();
  };

  const handleSubmit = () => {
    dispatch(resetPassword(props?.route?.params?.token, resetPasswordData, setAnimation, navigate));
  }

  return (
    <View style={styles.container}>
      <NewPasswordScreen
        handlePassword={handlePassword}
        resetPasswordData={resetPasswordData}
        handleSubmit={handleSubmit}
        animation={animation}
        navigate={navigate}
        goBack={goBack}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default NewPasswordContainer;
