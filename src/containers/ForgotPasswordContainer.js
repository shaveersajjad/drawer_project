import React, { useState } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import Toast from 'react-native-toast-message';
import { ScaledSheet } from 'react-native-size-matters';

import { ForgotPasswordScreen } from '../screens';
import { forgotPassword } from '../actions/auth';

const ForgotPasswordContainer = props => {
  const goBack = () => {
    const { navigation } = props;
    navigation.goBack();
  };

  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [animation, setAnimation] = useState(false);

  const navigate = token => {
    const { navigation } = props;
    navigation.navigate('DigitCode', { email, token });
  };

  const handleEmail = (name, value) => {
    setEmail(value);
  };

  const handleSubmit = () => {
    email !== ''
      ? dispatch(forgotPassword(email, setAnimation, navigate))
      : Toast.show({
        type: 'error',
        text1: 'Enter email yo reset password',
      });
  };

  return (
    <View style={styles.container}>
      <ForgotPasswordScreen
        email={email}
        handleEmail={handleEmail}
        handleSubmit={handleSubmit}
        animation={animation}
        navigate={navigate}
        goBack={goBack}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default ForgotPasswordContainer;
