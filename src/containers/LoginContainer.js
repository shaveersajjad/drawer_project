import React, {useState} from 'react';
import {View} from 'react-native';
import {useDispatch} from 'react-redux';
import {ScaledSheet} from 'react-native-size-matters';

import {LoginScreen} from '../screens';
import {login} from '../actions/auth';

const LoginContainer = props => {
  const goBack = () => {
    const {navigation} = props;
    navigation.goBack();
  };
  const dispatch = useDispatch();

  const [loginData, setLoginData] = useState({
    email: '',
    password: '',
  });
  const [animation, setAnimation] = useState(false);

  const navigate = (routeName, loginCheck) => {
    const {navigation} = props;
    loginCheck
      ? navigation.reset({
          index: 0,
          routes: [
            {
              name: routeName,
            },
          ],
        })
      : navigation.navigate(routeName);
  };

  const handleLoginData = (name, value) => {
    setLoginData({
      ...loginData,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    dispatch(login(loginData, navigate, setAnimation));
  };

  return (
    <View style={styles.container}>
      <LoginScreen
        loginData={loginData}
        handleLoginData={handleLoginData}
        handleSubmit={handleSubmit}
        animation={animation}
        navigate={navigate}
        goBack={goBack}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default LoginContainer;
