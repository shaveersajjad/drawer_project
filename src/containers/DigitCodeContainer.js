import React, { useState } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import Toast from 'react-native-toast-message';
import { ScaledSheet } from 'react-native-size-matters';

import { DigitCodeScreen } from '../screens';
import { forgotPassword } from '../actions/auth';

const DigitCodeContainer = props => {
  const goBack = () => {
    const { navigation } = props;
    navigation.goBack();
  };
  const dispatch = useDispatch();

  const [value, setValue] = useState('');
  const [animation, setAnimation] = useState(false);

  const navigate = routeName => {
    const { navigation } = props;
    navigation.navigate(routeName, {
      email: props?.route?.params?.email,
      token: props?.route?.params?.token,
    });
  };

  const handleSubmit = () => {
    value === props?.route?.params?.token
      ? navigate('NewPassword')
      : Toast.show({
        type: 'error',
        text1: 'OTP is not correct. Try again!',
      });
  };

  const handleSendAgain = () => {
    dispatch(
      forgotPassword(
        props?.route?.params?.email,
        setAnimation,
        navigate('DigitCode'),
      ),
    );
  };

  return (
    <View style={styles.container}>
      <DigitCodeScreen
        handleSubmit={handleSubmit}
        handleSendAgain={handleSendAgain}
        animation={animation}
        value={value}
        setValue={setValue}
        navigate={navigate}
        goBack={goBack}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
});

export default DigitCodeContainer;
