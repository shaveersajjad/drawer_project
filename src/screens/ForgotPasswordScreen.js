import React from 'react';
import {Text, View} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import {
  InputFieldComponent,
  LoginButtonComponent,
  ForgotPasswordHeader,
} from '../components';

const ForgotPasswordScreen = ({
  email,
  handleEmail,
  handleSubmit,
  animation,
  goBack,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.loginContentContainer}>
        <View>
          <ForgotPasswordHeader goBack={goBack} />
        </View>
        <Text style={styles.logInText}>Forgot Password</Text>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent
            placeholder="Email"
            name="email"
            value={email}
            handleChange={handleEmail}
          />
        </View>
        <View style={styles.nextBtnContainer}>
          <LoginButtonComponent
            title="Send Code"
            onPress={handleSubmit}
            animation={animation}
          />
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loginContentContainer: {
    backgroundColor: '#D2F7FF',
    borderBottomLeftRadius: '30@s',
    borderBottomRightRadius: '30@s',
    height: 'auto',
    paddingBottom: '100@s',
  },
  logInText: {
    textAlign: 'center',
    fontSize: '22@s',
    fontWeight: '600',
    marginTop: '40@s',
    marginBottom: '20@s',
  },
  inputFieldContainer: {
    marginTop: '30@s',
    marginLeft: '25@s',
    marginRight: '25@s',
  },
  nextBtnContainer: {
    width: '85%',
    alignSelf: 'center',
    marginTop: '50@s',
  },
});

export default ForgotPasswordScreen;
