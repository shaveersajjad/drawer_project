import React from 'react';
import {
  Text,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

import LogoImage from '../Images/MWB_logo.png';

const RouteCheckingScreen = () => {
  return (
    <View style={styles.contentContainer} >
      <Image style={styles.logoImage} source={LogoImage} />
      <Text style={styles.mwbText} >MWB</Text>
      <Text style={styles.mwbSubText} >Men's Well Being</Text>
      <ActivityIndicator color="#17C4EA" size='large' />
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImage: {
    height: '100@s',
    width: "110@s",
  },
  mwbText: {
    fontSize: '22@s',
    fontWeight: '500',
    marginTop: '10@s',
  },
  mwbSubText: {
    fontSize: '16@s',
    fontWeight: '300',
  },
});

export default RouteCheckingScreen;
