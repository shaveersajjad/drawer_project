import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';
import moment from 'moment';

import {
  MenuHeaderComponent,
  LoginButtonComponent,
  MindSetComponent,
  MindSetSongComponent,
  TimeLineComponent,
} from '../components';
import Icon from 'react-native-vector-icons/FontAwesome5';
import calender from '../Images/calender.png';
import blackCalander from '../Images/blackCalander.png';
import cookingImg from '../Images/cookingImg.png';
import meditationImg from '../Images/meditation.png';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Mindfulness',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Spiritual',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Focused',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91gujaa97f63',
    title: 'Spiritual',
  },
  {
    id: '58694a0f-3da1-471f-bd96-1455ihuj71e29d72',
    title: 'Focused',
  },
  {
    id: '3ac68afc-c05-48d3-a4f8-fbd91aa97f63',
    title: 'Mindfulness',
  },
  {
    id: '594a0f-3da1-471f-bd96-145571e29d72',
    title: 'Focused',
  },
];

const DATAONE = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3a28ba',
    title: 'Mindfulness',
    trackTime: '16:00',
    trackDescription: 'pay attention to your thoughts as they pass through your mind',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91f63',
    title: 'Spiritual',
    trackTime: '8:00',
    trackDescription: 'pay attention to your thoughts as they pass through your mind',
  },
  {
    id: '58694a0f-3da1-471f-bd96-14e29d72',
    title: 'Focused',
    trackTime: '9:00',
    trackDescription: 'pay attention to your thoughts as they pass through your mind',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91gu97f63',
    title: 'Spiritual',
    trackTime: '16:00',
    trackDescription: 'pay attention to your thoughts as they pass through your mind',
  },
  {
    id: '58694a0f-3da1-471f-bd96-1455i1e29d72',
    title: 'Focused',
    trackTime: '11:00',
    trackDescription: 'pay attention to your thoughts as they pass through your mind',
  },
];

const HomeScreen = ({ goBack, navigate, meals, openDrawer }) => {
  const renderItem = ({ item }) => (
    <MindSetComponent onPress={() => navigate('Meditation')} title={item.title} />
  );

  const renderItemOne = ({ item }) => (
    <MindSetSongComponent
      onPress={() => navigate('AboutUs')}
      title={item.title}
      trackTime={item.trackTime}
      trackDescription={item.trackDescription}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      <MenuHeaderComponent openDrawer={openDrawer} headerTitle="MWB" navigate={navigate} />
      <ScrollView>
        <View style={styles.contentFlex}>
          <View>
            <Text style={styles.todayMealText}>Today's Meal Plan</Text>
            <View style={styles.dateContent}>
              <Icon style={styles.blackCalanderImg} name="calendar-alt" size={18} color="black" />
              <Text>{moment(meals?.date).format('DD/MM/YYYY')}</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => navigate('Calander')} style={styles.calanderBackground}>
            <Icon name="calendar-alt" size={20} color="white" />
          </TouchableOpacity>
        </View>
        {Object.entries(meals).length === 0 ? (
          <View>
            <Image style={styles.cookingImg} source={cookingImg} />
            <View style={styles.btnContainer}>
              <LoginButtonComponent onPress={() => navigate('PlanMeal')} title="Plan Meal" />
            </View>
          </View>
        ) : (
          <View style={{ flex: 1, margin: 10 }}>
            <TimeLineComponent meals={meals} />
          </View>
        )}
        <View style={styles.meditationContentBar}>
          <Image style={styles.meditationImg} source={meditationImg} />
          <Text style={styles.meditationText}>Meditation</Text>
        </View>
        <View style={{ margin: 10 }}>
          <FlatList
            horizontal={true}
            data={DATA}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={{ margin: 10 }}>
          <FlatList data={DATAONE} renderItem={renderItemOne} keyExtractor={(item) => item.id} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentFlex: {
    flexDirection: 'row',
    padding: '15@s',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  calanderBackground: {
    backgroundColor: '#17C4EA',
    height: '30@s',
    width: '30@s',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '4@s',
  },
  calenderImg: {
    height: '15@s',
    width: '15@s',
  },
  dateContent: {
    flexDirection: 'row',
    marginTop: '5@s',
    alignItems: 'center',
  },
  blackCalanderImg: {
    marginRight: '6@s',
  },
  cookingImg: {
    height: '150@s',
    width: '150@s',
    alignSelf: 'center',
    marginTop: '30@s',
  },
  btnContainer: {
    width: '50%',
    alignSelf: 'center',
    marginTop: '20@s',
  },
  meditationContentBar: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '30@s',
    paddingLeft: '15@s',
  },
  meditationText: {
    fontSize: '14@s',
    fontWeight: '400',
    color: '#00404E',
  },
  todayMealText: {
    fontSize: '14@s',
    fontWeight: '500',
  },
});

export default HomeScreen;
