import React, { useState, useRef } from 'react';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ScaledSheet } from 'react-native-size-matters';
import InputFieldComponent from '../components/InputFieldComponent';
import LoginButtonComponent from '../components/LoginButtonComponent';

const SignUpScreen = ({ signUpData, handleSignUpData, handleSubmit, navigate, animation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.loginContentContainer}>
        <Text style={styles.logInText}>Sign Up</Text>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent placeholder="Name" name="name" value={signUpData.name} handleChange={handleSignUpData} />
        </View>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent placeholder="Email" name="email" value={signUpData.email} handleChange={handleSignUpData} />
        </View>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent placeholder="Password" name="password" value={signUpData.password} handleChange={handleSignUpData} />
        </View>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent placeholder="Confirm Password" name="confirmPassword" value={signUpData.confirmPassword} handleChange={handleSignUpData} />
        </View>
        <View style={styles.btnContainer}>
          <LoginButtonComponent title="Sign Up" onPress={handleSubmit} animation={animation} />
        </View>
      </View>
      <View style={styles.contentFlex}>
        <Text style={styles.memberText}>Already registered? </Text>
        < TouchableOpacity onPress={() => navigate('Login')} >
          <Text style={styles.signUpText}> Log in</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loginContentContainer: {
    backgroundColor: '#D2F7FF',
    // flex: 1,
    padding: '10@s',
    borderBottomLeftRadius: '30@s',
    borderBottomRightRadius: '30@s',
    height: 'auto',
    paddingBottom: '40@s',
  },
  logInText: {
    textAlign: 'center',
    fontSize: '22@s',
    fontWeight: '600',
    marginTop: '40@s',
    marginBottom: '20@s',
  },
  inputFieldContainer: {
    marginTop: '12@s',
    marginLeft: '25@s',
    marginRight: '25@s',
  },
  btnContainer: {
    marginTop: '40@s',
    marginLeft: '25@s',
    marginRight: '25@s',
  },
  contentFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '60@s',
  },
  memberText: {
    fontSize: '12@s',
    fontWeight: '400',
  },
  signUpText: {
    fontSize: '13@s',
    fontWeight: '400',
    color: 'red',
  },
});

export default SignUpScreen;
