import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { ScaledSheet } from 'react-native-size-matters';

import {
  LoginButtonComponent,
  ForgotPasswordHeader,
  DigitCodeComponent,
} from '../components';

const DigitCodeScreen = ({
  handleSubmit,
  handleSendAgain,
  animation,
  value,
  setValue,
  goBack,
}) => {
  return (
    <View style={styles.container}>
      <ForgotPasswordHeader goBack={goBack} />
      <Text style={styles.enterOTP}>Enter OTP</Text>
      <View style={styles.backgroundContainer}>
        <View>
          <DigitCodeComponent setValue={setValue} />
        </View>
        <View style={styles.btnContainer}>
          <LoginButtonComponent
            title="Confirm"
            onPress={handleSubmit}
            animation={animation}
          />
        </View>
        <View style={{ alignSelf: 'center' }}>
          <Text>Didn't recieve it?</Text>
          <TouchableOpacity onPress={() => handleSendAgain()}>
            <Text style={styles.sendItAgain}>Sent it again</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  enterOTP: {
    fontSize: '22@s',
    fontWeight: '700',
    alignSelf: 'center',
    marginTop: '30@s',
  },
  backgroundContainer: {
    backgroundColor: '#D2F7FF',
    flex: 1,
    marginTop: '100@s',
    justifyContent: 'space-between',
    borderTopRightRadius: '30@s',
    borderTopLeftRadius: '30@s',
    paddingBottom: '100@s',
  },
  btnContainer: {
    width: '85%',
    alignSelf: 'center',
  },
  sendItAgain: {
    textAlign: 'center',
    color: '#17C4EA',
  },
});

export default DigitCodeScreen;
