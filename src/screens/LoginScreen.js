import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';

import LogoImage from '../Images/MWB_logo.png';
import {InputFieldComponent} from '../components';
import {LoginButtonComponent} from '../components';

const LoginScreen = ({
  loginData,
  handleLoginData,
  handleSubmit,
  animation,
  navigate,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.logoImageContainer}>
        <Image style={styles.logoImage} source={LogoImage} />
        <Text style={styles.logoHeading}>MWB</Text>
        <Text style={styles.logoSubHeading}>Men's Well Being</Text>
      </View>
      <View style={styles.loginContentContainer}>
        <View style={styles.contentFlex}>
          <Text style={styles.memberText}>Not a member? </Text>
          <TouchableOpacity onPress={() => navigate('SignUp')}>
            <Text style={styles.signUpText}> Sign up</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.logInText}>Log in</Text>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent
            placeholder="Email"
            name="email"
            value={loginData.email}
            handleChange={handleLoginData}
          />
        </View>
        <View style={styles.inputFieldContainer}>
          <InputFieldComponent
            placeholder="Password"
            name="password"
            value={loginData.password}
            handleChange={handleLoginData}
          />
        </View>
        <View style={styles.btnContainer}>
          <LoginButtonComponent
            title="Log In"
            onPress={handleSubmit}
            animation={animation}
          />
        </View>
        <TouchableOpacity onPress={() => navigate('ForgotPassword')}>
          <Text style={styles.forgetPasswordText}>Forget Pasword?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  logoImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '30@s',
  },
  logoImage: {
    height: '120@s',
    width: '110@s',
  },
  logoHeading: {
    fontSize: '24@s',
    fontWeight: '400',
  },
  logoSubHeading: {
    fontSize: '14@s',
    fontWeight: '300',
  },
  loginContentContainer: {
    backgroundColor: '#D2F7FF',
    flex: 1,
    marginTop: '40@s',
    padding: '10@s',
    borderTopLeftRadius: '30@s',
    borderTopRightRadius: '30@s',
  },
  contentFlex: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  memberText: {
    fontSize: '12@s',
    fontWeight: '400',
  },
  signUpText: {
    fontSize: '13@s',
    fontWeight: '400',
    color: 'red',
  },
  logInText: {
    textAlign: 'center',
    fontSize: '22@s',
    fontWeight: '600',
    marginTop: '30@s',
    marginBottom: '20@s',
  },
  inputFieldContainer: {
    marginTop: '12@s',
    marginLeft: '25@s',
    marginRight: '25@s',
  },
  btnContainer: {
    marginTop: '40@s',
    marginLeft: '25@s',
    marginRight: '25@s',
  },
  forgetPasswordText: {
    color: '#717171',
    fontSize: '10@s',
    fontWeight: '500',
    textAlign: 'center',
    marginTop: '30@s',
    textDecorationLine: 'underline',
  },
});

export default LoginScreen;
