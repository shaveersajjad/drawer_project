import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import {
  LoginContainer,
  SignUpContainer,
  HomeContainer,
  ForgotPasswordContainer,
  DigitCodeContainer,
  NewPasswordContainer,
  RouteCheckingContainer,
} from '../containers';
import DrawercontentData from '../components/DrawerContentData';

const Stack = createStackNavigator();
const AuthStack = createStackNavigator();

const Auth = () => {
  return (
    <AuthStack.Navigator initialRouteName="RouteChecking">
      <Stack.Screen
        name="RouteChecking"
        component={RouteCheckingContainer}
        options={{ headerShown: false }}
      />
      <AuthStack.Screen component={LoginContainer} name="Login" options={{ headerShown: false }} />
      <AuthStack.Screen
        component={SignUpContainer}
        name="SignUp"
        options={{ headerShown: false }}
      />
      <AuthStack.Screen
        component={ForgotPasswordContainer}
        name="ForgotPassword"
        options={{ headerShown: false }}
      />
      <AuthStack.Screen name="Home" component={MyDrawer} options={{ headerShown: false }} />
    </AuthStack.Navigator>
  );
};

const App = () => {
  return (
    <Stack.Navigator initialRouteName="HomeContainer">
      <Stack.Screen
        component={HomeContainer}
        name="HomeContainer"
        options={{ headerShown: false }}
      />
      <Stack.Screen
        component={DigitCodeContainer}
        name="DigitCode"
        options={{ headerShown: false }}
      />
      <Stack.Screen
        component={NewPasswordContainer}
        name="NewPassword"
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const Drawer = createDrawerNavigator();
const MyDrawer = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => {
        return <DrawercontentData {...props} />;
      }}
    >
      <Drawer.Screen name="Home" component={App} />
    </Drawer.Navigator>
  );
};

const MainScreen = createSwitchNavigator(
  {
    Auth: {
      screen: Auth,
    },
    Home: {
      screen: App,
    },
    Drawer: {
      screen: MyDrawer,
    },
  },
  {
    initialRouteName: 'Auth',
  }
);

export default createAppContainer(MainScreen);
