import Toast from 'react-native-toast-message';
import axios from 'axios';

import { Api } from '../Utils/Api';
import * as types from './types';

const meals = data => {
  return {
    type: types.GET_ALL_MEALS,
    meals: data
  }
};

export const getAllMeals = userId => dispatch => {
  axios
    .get(`${Api}/mealPlan/`)
    .then(res => {
      dispatch(meals(res?.data?.data?.[0]?.mealPlan));
    })
    .catch(err => {
      dispatch(meals({}));
    });
};