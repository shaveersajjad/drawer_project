import Toast from 'react-native-toast-message';
import axios from 'axios';

import { Api } from '../Utils/Api';
import Storage from '../Utils/Storage';
import * as types from './types';

const localStorage = async (currentUser, token) => {
  await Storage.storeData('currentUser', currentUser);
  await Storage.storeData('token', token);
};

const user = data => {
  return {
    type: types.USER,
    user: data
  }
};

export const login = (loginData, navigate, setAnimation) => dispatch => {
  setAnimation(true);
  axios
    .post(`${Api}/user/login`, loginData)
    .then(res => {
      localStorage(res?.data?.data, res?.data?.token);
      setAnimation(false);
      navigate('Home', true);
    })
    .catch(err => {
      setAnimation(false);
      Toast.show({
        type: 'error',
        text1: err.response.data.errorMessage,
      });
    });
};

export const signUp = (signUpData, navigate, setAnimation) => dispatch => {
  setAnimation(true);
  axios
    .post(`${Api}/user/signup`, signUpData)
    .then(res => {
      setAnimation(false);
      localStorage(res?.data?.data, res?.data?.token);
      navigate('Home');
    })
    .catch(err => {
      Toast.show({
        type: 'error',
        text1: err?.response?.data?.errorMessage,
      });
      setAnimation(false);
    });
};

export const forgotPassword = (email, setAnimation, navigate) => dispatch => {
  setAnimation(true);
  axios
    .post(`${Api}/user/forgetPassword`, { email })
    .then(res => {
      setAnimation(false);
      Toast.show({
        type: 'success',
        text1: `OTP has been sent to ${email}.`,
      });
      navigate(res?.data?.token);
    })
    .catch(err => {
      setAnimation(false);
      Toast.show({
        type: 'error',
        text1: err?.response?.data?.message,
      });
    });
};

export const resetPassword = (token, resetPasswordData, setAnimation, navigate) => dispatch => {
  setAnimation(true);
  axios
    .patch(`${Api}/user/resetPassword/${token}`, resetPasswordData)
    .then(res => {
      setAnimation(false);
      Toast.show({
        type: 'success',
        text1: res?.data?.message,
      });
      navigate('Login');
    })
    .catch(err => {
      setAnimation(false);
      Toast.show({
        type: 'error',
        text1: res?.data?.message,
      });
    });
};


export const getUserData = userId => dispatch => {
  axios
    .get(`${Api}/user/${userId}`)
    .then(res => {
      dispatch(user(res?.data?.data));
    })
    .catch(err => {
    });
};

export const updateName = (userId, name, setName) => dispatch => {
  axios
    .patch(`${Api}/user/${userId}`, { name })
    .then(res => {
      dispatch(user(res?.data?.data));
      setName("");
    })
    .catch(err => { })
};

export const updateProfilePic = (userId, image) => dispatch => {
  axios
    .patch(`${Api}/user/${userId}`, { image })
    .then(res => {
      dispatch(user(res?.data?.data));
    })
    .catch(err => { })
};

export const uploadProfilePic = (formData, userId) => dispatch => {
  axios
    .post(`${Api}/image/upload`, formData)
    .then(res => {
      dispatch(updateProfilePic(userId, res?.data?.data?.[0]));
    })
    .catch(err => { });
};
