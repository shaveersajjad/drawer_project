import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Provider } from 'react-redux';
import Toast from 'react-native-toast-message';
import { NavigationContainer } from '@react-navigation/native';

import axios from 'axios';

import Routes from './src/Utils/Routes';
import store from './src/store/store';
import Storage from './src/Utils/Storage';

const App = () => {
  axios.interceptors.request.use(async config => {
    await Storage.retrieveData('token')
      .then(token => {
        config.headers = {
          "X-Auth-Token": token,
        }
      });
    return config;
  });

  return (
    <Provider store={store}>
      <SafeAreaView style={styles.container}>
        <NavigationContainer>
          <Routes />
          <Toast position="top" ref={ref => Toast.setRef(ref)} />
        </NavigationContainer>
      </SafeAreaView>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;